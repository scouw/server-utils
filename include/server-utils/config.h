#pragma once

#include <nlohmann/json.hpp>
#include <vector>
#include <map>
#include <BLRevive/Config.h>
#include <BLRevive/Utils.h>

/**
 * @brief configuration of server properties (url key: `blre.server.props`)
*/
struct ServerProperties {
	// can't set this early enough for it to take effect
	// float MaxIntermissionIdle;

	// list of random bot names to use
	std::vector<std::string> RandomBotNames = {};
	// timespan player has to wait to respawn after dying
	float GameRespawnTime = 10.0f;
	// timespan after player will be spawned without submission after dying
	float GameForceRespawnTime = 30.0f;
	// 
	float GameSpectatorSwitchDelayTime = 120.0f;
	// number of votes required to kick player of enemy team
	int NumEnemyVotesRequiredForKick = 4;
	// number of votes required to kick player of own team
	int NumFriendlyVotesRequiredForKick = 2;
	// how long a player is banned from server after vote kick (in seconds)
	int VoteKickBanSeconds = 1200;
	// maximum time before player is marked as idle
	float MaxIdleTime = 180.0f;
	// required player count to start a match
	int MinRequiredPlayersToStart = 1;
	// timespan the server will wait in lobby for other players
	float PlayerSearchTime = 30.0f;
	// default time limit (in minutes) for a match
	int TimeLimit = 10;
	// default goal score for a match
	int GoalScore = 3000;
	// disable intermission idle kick
	bool KickOnIdleIntermission = false;

	// maximum allowed bots
	int MaxBotCount = 0;
	// maxmium players
	int MaxPlayers = 16;
	int NumBots = 0;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ServerProperties,
	RandomBotNames, GameRespawnTime, GameForceRespawnTime, GameSpectatorSwitchDelayTime,
	NumEnemyVotesRequiredForKick, NumFriendlyVotesRequiredForKick,
	VoteKickBanSeconds, MaxIdleTime, MinRequiredPlayersToStart, PlayerSearchTime,
	TimeLimit, GoalScore, KickOnIdleIntermission, MaxBotCount, MaxPlayers, NumBots);
MAP_URL_CONFIG(ServerProperties, blre.server.props,
	GameRespawnTime, GameForceRespawnTime, GameSpectatorSwitchDelayTime,
	NumEnemyVotesRequiredForKick, NumFriendlyVotesRequiredForKick,
	VoteKickBanSeconds, MaxIdleTime, MinRequiredPlayersToStart, PlayerSearchTime,
	TimeLimit, GoalScore);

/**
 * @brief configuration of server mutators (url key: `blre.server.mutators`)
*/
struct ServerMutators {
	// disable depot usage
	bool DisableDepots = false;
	// disable HRV
	bool DisableHRV = false;
	// disable headshots
	bool DisableHeadShots = false;
	// only default loadouts
	bool StockLoadout = false;
	// disable primary weapons
	bool DisablePrimaries = false;
	// disable secondary weapons
	bool DisableSecondaries = false;
	// disable gear
	bool DisableGear = false;
	// disable tactical gear
	bool DisableTacticalGear = false;
	// disable health regenaration
	bool DisableHealthRegen = false;
	// disable elemental ammo
	bool DisableElementalAmmo = false;
	// only headshots
	bool HeadshotsOnly = false;

	// stamina modifier (mulitiplied)
	float StaminaModifier = 1.0f;
	// health modifier (multiplied)
	float HealthModifier = 1.0f;
};

MAP_JSON_URL_CONFIG(ServerMutators, blre.server.mutators,
	DisableDepots, DisableHRV, DisableHeadShots, StockLoadout, DisablePrimaries, DisableSecondaries,
	DisableGear, DisableTacticalGear, DisableHealthRegen, DisableElementalAmmo, HeadshotsOnly);

/**
 * @brief configuration of web server endpoint (url key: `blre.server.webserver`)
*/
struct ServerHTTPEndpoint {
	// wether web server is enabled
	bool Enable = true;
	// host string of web server (without port)
	std::string Host = "0.0.0.0";
	// URI for server info
	std::string InfoURI = "/server_info";
	bool ShowSpectatorTeam = false;
	// URI for patch file
	std::string PatchesURI = "/patches";
};

MAP_JSON_URL_CONFIG(ServerHTTPEndpoint, blre.server.webserver,
	Enable, Host, InfoURI, PatchesURI);

/**
 * @brief configuration of server information (url key: `blre.server.info`)
*/
struct ServerInfoConfig
{
	// wether to serve server info per http
	bool Serve = true;
	// ip/domain of web server which serves the info (0.0.0.0 for local)
	std::string Host = "0.0.0.0";
	// port of web server which serves the info
	int Port = 7778;
	// URI path where to serve the server info
	std::string URI = "/server_info";
	// info file name
	std::string FileName = "server_info";
	// wether to show spectator team in list
	bool ShowSpectatorTeam = false;
};

MAP_JSON_URL_CONFIG(ServerInfoConfig, blre.server.info,
	Serve, Host, Port, URI, FileName,
	ShowSpectatorTeam);

struct ValidationRules
{
	std::vector<int> DisabledItems = {};
	std::map<int, std::vector<int>> DisabledWeaponMods = {};
	std::map<int, std::vector<int>> DisabledArmorPairs = {};
};

inline void from_json(const nlohmann::json& j, ValidationRules& vr)
{
	if(j.contains("DisabledItems"))
		j["DisabledItems"].get_to(vr.DisabledItems);

	if(j.contains("DisabledWeaponMods"))
		for (auto& [weaponUID, modUIDs] : j["DisabledWeaponMods"].get<std::map<std::string, std::vector<int>>>())
			vr.DisabledWeaponMods[std::stoi(weaponUID)] = modUIDs;

	if(j.contains("DisabledArmorPairs"))
		for (auto& [armorUID, armorUIDs] : j["DisabledArmorPairs"].get<std::map<std::string, std::vector<int>>>())
			vr.DisabledArmorPairs[std::stoi(armorUID)] = armorUIDs;
}

inline void to_json(nlohmann::json& j, const ValidationRules& vr)
{
	j["DisabledItems"] = vr.DisabledItems;

	for (auto& [weaponUID, modUIDs] : vr.DisabledWeaponMods)
		j["DisabledWeaponMods"][std::to_string(weaponUID)] = modUIDs;

	for (auto& [armorUID, armorUIDs] : vr.DisabledArmorPairs)
		j["DisabledArmorPairs"][std::to_string(armorUID)] = armorUIDs;
}


struct PlaylistEntry {
	std::string GameMode;
	std::string Map;
	ServerProperties Properties = {};
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(PlaylistEntry,
	GameMode, Map, Properties);

/**
 * @brief configuration of server (url key: `blre.server`)
*/
struct ServerConfig {
	// server properties
	ServerProperties properties;
	// mutators applied to all matches
	ServerMutators mutators;
	// server info config
	//ServerInfoConfig info;
	ServerHTTPEndpoint webserver;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE_WITH_DEFAULT(ServerConfig,
	properties, mutators, webserver);

struct ServerUtilsConfig
{
	// base path to server utils configurations
	std::string BasePath = BLRE::Utils::FS::BlreviveConfigPath() + "server_utils/";
	
	// path to config directory
	std::string ConfigsPath = BasePath + "configs/";
	// path to config file
	std::string ConfigFile = ConfigsPath + "{ServerName}.json";

	// path to patch configs
	std::string PatchesPath = BasePath + "patches/";
	// path to validation rule configs
	std::string RulesPath = BasePath + "rules/";
	// path to playlist configs
	std::string PlaylistsPath = BasePath + "playlists/";
	// path to server info
	std::string InfoPath = BasePath + "info/";

	// path to server info file
	std::string InfoFile = InfoPath + "{ServerName}.json";
	// path to server rules config
	std::string RulesFile = RulesPath + "{ServerName}.json";
	// path to server patches config
	std::string PatchFile = PatchesPath + "{ServerName}.json";
};

MAP_JSON_URL_CONFIG(ServerUtilsConfig, blre.server,
	BasePath, ConfigsPath, ConfigFile, PatchesPath, RulesPath, PlaylistsPath,
	InfoPath, InfoFile, RulesFile, PatchFile);