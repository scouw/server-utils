#pragma once

#include <httplib.h>

// NOTE: Pulled from both Network.h and Utils.h from previous proxy version
// https://gitlab.com/blrevive/tools/proxy/-/blob/9a4bc15d439e48ba6fa192dda961b20bd42dd936/include/Proxy/Network.h
// https://gitlab.com/blrevive/tools/proxy/-/blob/9a4bc15d439e48ba6fa192dda961b20bd42dd936/include/Proxy/Utils.h

namespace BLRE::Network
{
	enum class RequestType { GET, POST };

	template <typename T>
	class ProcessSingleton
	{
	public:
		/**
		 * Get current instance of singleton (may return null)
		 *
		 * @return pointer to instance
		 */
		inline static std::shared_ptr<T> GetInstance() { return _self; }

		/**
		 * Create instance of T as singleton
		 *
		 * @tparam ...Args types of constructor params
		 * @param args constructor params
		 * @return instance of T
		 */
		template <typename... Args>
		inline static std::shared_ptr<T> Create(Args... args)
		{
			if (_self == NULL)
				_self = std::make_shared<T>(args...);
			return _self;
		}

	protected:
		inline static std::shared_ptr<T> _self = NULL;
	};

	/**
	* Server manager
	*/
	class HTTPServer : public ProcessSingleton<HTTPServer>
	{
	public:
		using Handler = httplib::Server::Handler;

		/**
			* Add connection handler to server.
			* Only works in InitializeModule function!
			*
			* @param  type 		type of request (GET|POST)
			* @param  pattern 		route to listen
			* @param  handler 		connection handler
			*/
		inline void AddConnectionHandler(RequestType type, std::string pattern, Handler handler)
		{
			if (bStarted)
				throw "Can't add connection handler when server is started!";

			switch (type) {
			case RequestType::GET:
				_Server.Get(pattern, handler);
				break;
			case RequestType::POST:
				_Server.Post(pattern, handler);
				break;
			}
		};

		/**
			* Start server in new thread
			*/
		void Start()
		{
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)StartListen, this, NULL, NULL);
			bStarted = true;
		}

		/**
			* Start listening for connections
			*/
		static void StartListen() { _self->Listen(); }

		/**
			* Listen for connections
			*/
		void Listen()
		{
			try {
				_Server.listen(_Host.c_str(), _Port);
			}
			catch (int e) {
				throw "Couldn't start server on " + _Host + ":" + std::to_string(_Port);
			}
		}

		HTTPServer(std::string Host, int Port) : _Host(Host), _Port(Port) {}
	private:
		bool bStarted = false;
		httplib::Server _Server;
		std::string _Host;
		int _Port;
	};
}