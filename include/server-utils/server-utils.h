#pragma once

#include <BLRevive/BLRevive.h>
#include <server-utils/webserver.h>
#include <server-utils/config.h>
#include <server-utils/server-info.h>
#include <BLRevive/Detours/FunctionDetour.h>
#include <server-utils/loadout-validator.h>
#include <BLRevive/Components/Patcher.h>

// logger instance of this module
static BLRE::LogFactory::TSharedLogger Log;

/**
 * @brief utilities for servers
*/
class ServerUtils : BLRE::Component
{
public:
	ServerUtils(BLRE::BLRevive* blreInstance) 
		: BLRE::Component("ServerUtils")
	{
		blre = blreInstance;
	}

	/**
	 * @brief initialize the server utils
	*/
	void Init();

	/**
	 * @brief load configuration file
	 * @param configFilePath path to config file
	 * @return ServerConfig configuration
	*/
	ServerConfig LoadConfig(std::string configFilePath);
	
	/**
	 * @brief write server info to json
	*/
	void SaveServerInfo();

	/**
	 * @brief handler for web server info request
	 * @param req request
	 * @param res response (server info as json)
	*/
	void ServerInfoRequestHandler(const httplib::Request& req, httplib::Response& res);

	void ServerPatchesRequestHandler(const httplib::Request& req, httplib::Response& res);

	// server configuration
	ServerConfig serverConfig;
	// module configuration
	ServerUtilsConfig moduleConfig;
private:
	// server infos
	ServerInfo info;
	// instance of blrevive api
	BLRE::BLRevive* blre;
	// loadout validator
	BLRE::LoadoutValidator LoadoutValidator = { this };
	std::string ServerName;

	std::vector<PlaylistEntry> Playlist = {};
private:
	// mutex for server infos (necessary since webserver runs in own thread)
	static inline HANDLE infoMutex = NULL;

	void CheckDefaultFiles();

	/**
	 * @brief update the server info
	 * @param game current game instance
	*/
	void UpdateServerInfo(AFoxGame* game);

	/**
	 * @brief apply default settings (should only run once)
	*/
	void ApplyDefaultSettings();

	/**
	 * @brief apply settings which needs periodic refresh
	 * @param game current game instance
	*/
	void ApplySettings(AFoxGame* game);

	/**
	 * @brief setup custom playlist
	*/
	void InitializePlaylist();

	void DETOUR_CB_CLS_EVENT(RegisterCustomPlaylist, UFoxDataStore_Playlists, Registered);

	/**
	 * @brief detour for FoxGame.FoxGame.UpdateGameSettings which will update server info
	 * @param detour instance of detour
	 * @param game current game instance
	 * @param parms parameters of FoxGame.FoxGame.UpdateGameSettings
	*/
	void DETOUR_CB_CLS(UpdateGameSettingsCb, AFoxGame, UpdateGameSettings);

	/**
	 * @brief detour for FoxGame.FoxPC.ServerSetLoadout which will validate the user loadout
	 * @param  detour instance of detour
	 * @param  pc player to validate loadout for
	 * @param  parms parameters
	*/
	void DETOUR_CB_CLS(ValidatePlayerLoadout, AFoxPC, ServerSetLoadout);

	/**
	 * @brief detour for FoxGame.FoxDataStore_Playlists.SelectPlaylistByName to inject custom playlist
	 * @param  detour instance of detour
	 * @param  plds instance of playlist datastore
	 * @param  parms parameters
	*/
	bool DETOUR_CB_CLS_EVENT(SelectPlaylistByNameCb, UFoxDataStore_Playlists, SelectPlaylistByName);
};