#pragma warning(disable:4244)
#define WIN32_LEAN_AND_MEAN
#define NOMINMAX
#include <windows.h>
#include <SdkHeaders.h>
#include <server-utils/server-utils.h>
#include <server-utils/webserver.h>
#include <server-utils/server-info.h>
#include <fstream>
#include <filesystem>
#include <BLRevive/Detours/FunctionDetour.h>

using namespace BLRE;
using namespace BLRE::Detours;

namespace fs = std::filesystem;

using namespace fmt::literals;

void ServerUtils::Init()
{
	Log->info("Initializing ServerUtils");

	CheckDefaultFiles();

	// load module config (from main config file)
	moduleConfig = blre->Config.Modules["server-utils"].get<ServerUtilsConfig>();
	map_url_config(moduleConfig, blre->URL);

	ServerName = blre->URL.GetParam("ServerName", "default");

	// format config file strings
	#define ResolveFilePath(filePath, basePath) \
		filePath = fmt::format(filePath, "ServerName"_a=ServerName); \
		if(fs::path(filePath).is_relative()) filePath = basePath + filePath;
	
	ResolveFilePath(moduleConfig.ConfigFile, moduleConfig.ConfigsPath);
	ResolveFilePath(moduleConfig.RulesFile, moduleConfig.RulesPath);
	ResolveFilePath(moduleConfig.PatchFile, moduleConfig.PatchesPath);
	ResolveFilePath(moduleConfig.InfoFile, moduleConfig.InfoPath);

	// load server config
	serverConfig = LoadConfig(moduleConfig.ConfigFile);
	info = {};
	infoMutex = CreateMutexA(NULL, false, NULL);

	blre->Patcher->LoadPatchConfig(moduleConfig.PatchFile);
	InitializePlaylist();

	// initialize loadout validator
	this->LoadoutValidator.BuildItemClassUIDMap();
	this->LoadoutValidator.LoadValidationMapping();
	this->LoadoutValidator.LoadValidationRules();

	auto dtUpdateGameSettings = FunctionDetour::Create("FoxGame.FoxGame.UpdateGameSettings", this, &ServerUtils::UpdateGameSettingsCb);
	dtUpdateGameSettings->Enable();

	auto dtServerSetLoadout = FunctionDetour::Create("FoxGame.FoxPC.ServerSetLoadout", this, &ServerUtils::ValidatePlayerLoadout);
	dtServerSetLoadout->Enable();

	if(serverConfig.webserver.Enable) {
		auto webserver = Network::HTTPServer::Create("0.0.0.0", blre->URL.GetParam("Port", 7777));
	
		webserver->AddConnectionHandler(Network::RequestType::GET, serverConfig.webserver.PatchesURI,
			[this](const httplib::Request& req, httplib::Response& res) {
				this->ServerPatchesRequestHandler(req, res);
			});

		webserver->AddConnectionHandler(Network::RequestType::GET, serverConfig.webserver.InfoURI,
			[this](const httplib::Request& req, httplib::Response& res) {
				this->ServerInfoRequestHandler(req, res);
			});

		webserver->Start();
	}

	// onslaught fix (allow players with zero netid)
	auto dtIsNetIdAllowed = FunctionDetour::Create("FoxGame.FoxGRI_OS.IsNetIdAllowed",
		+[](DETOUR_FUNC_ARGS_IMPL(AFoxGRI_OS, IsNetIdAllowed, gri))
		{
			detour->Continue();
			return true;
		});
	dtIsNetIdAllowed->Enable();
}

void ServerUtils::CheckDefaultFiles()
{
	ServerUtilsConfig defaultConfig;

	if (!fs::exists(defaultConfig.BasePath))
		fs::create_directories(defaultConfig.BasePath);
	if (!fs::exists(defaultConfig.PatchesPath))
		fs::create_directories(defaultConfig.PatchesPath);
	if (!fs::exists(defaultConfig.ConfigsPath))
		fs::create_directories(defaultConfig.ConfigsPath);
	if (!fs::exists(defaultConfig.RulesPath))
		fs::create_directories(defaultConfig.RulesPath);
	if (!fs::exists(defaultConfig.PlaylistsPath))
		fs::create_directories(defaultConfig.PlaylistsPath);
	if (!fs::exists(defaultConfig.InfoPath))
		fs::create_directories(defaultConfig.InfoPath);
}


FName CreateName(const char* name)
{
	FName fname;
	fname.Index = FName::Names()->Count;
	FNameEntry* entry = new FNameEntry(name, fname.Index);
	FName::Names()->push_back(entry);
	return fname;
}

FName FindOrCreateName(const char* name)
{
	for (FNameEntry* nameEntry : *FName::Names()) {
		if (!nameEntry)
			continue;

		if (!stricmp(nameEntry->Name, name))
			return FName(nameEntry->Index());
	}

	return CreateName(name);
}

void DETOUR_CB_CLS_EVENT_IMPL(ServerUtils::RegisterCustomPlaylist, UFoxDataStore_Playlists, Registered, ds)
{
	detour->Continue();

	std::string playlistName = blre->URL.GetParam("Playlist", "");
	// add playlist entry to provider
	auto playlistProvider = UObject::CreateInstance<UFoxDataProvider_Playlist>();
	playlistProvider->FriendlyName = "Awesome Playlist";
	playlistProvider->NetIndex = -1;
	playlistProvider->Name = FindOrCreateName(playlistName.c_str());
	playlistProvider->bRandomCycle = false;
	playlistProvider->RequiredChunks.push_back(23);
	for (auto playlistEntry : this->Playlist) {
		playlistProvider->PlaylistPairings.push_back({ 
			FindOrCreateName(playlistEntry.Map.c_str()), FindOrCreateName(playlistEntry.GameMode.c_str()) });
	}
	playlistProvider->eventInitializeProvider(false);
	ds->PlayListProviders.push_back(playlistProvider);
}

void ServerUtils::InitializePlaylist()
{
	// get playlist name from url
	std::string playlistName = blre->URL.GetParam("Playlist", "");
	if (playlistName.empty()) {
		Log->info("using default playlist");
		return;
	}

	// load playlist config
	std::string playlistConfigFile = moduleConfig.PlaylistsPath + playlistName + ".json";
	if (!fs::exists(playlistConfigFile)) {
		Log->info("custom playlist file {} not found, using default", playlistName);
		return;
	}

	Log->info("loading playlist {}", playlistName);
	nlohmann::json playlist = Utils::ReadJsonFile<std::vector<nlohmann::json>>(playlistConfigFile);

	// merging playlist properties with server properties
	nlohmann::json jServerProps = serverConfig.properties;
	for (auto& playlistEntry : playlist) {
		nlohmann::json jEntryProps = jServerProps;
		jEntryProps.merge_patch(playlistEntry["Properties"]);
		playlistEntry["Properties"] = jEntryProps;
		Playlist.push_back(playlistEntry);
	}

	// setup registered 
	auto dtPlaylistDSRegistered = BLRE::Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.Registered", 
		this, &ServerUtils::RegisterCustomPlaylist);
	dtPlaylistDSRegistered->Enable();

	auto dtSelectPlaylistByName = BLRE::Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.SelectPlaylistByName",
		this, &ServerUtils::SelectPlaylistByNameCb);
	dtSelectPlaylistByName->Enable();

	auto dtCyclePlaylist = Detours::FunctionDetour::Create("FoxGame.FoxDataStore_Playlists.CyclePlaylist",
		+[](DETOUR_FUNC_ARGS_IMPL(UFoxDataStore_Playlists, CyclePlaylist, pds)) 
		{
			detour->Skip();
			pds->PlaylistCycleIndex++;
			if (pds->PlaylistCycleIndex >= pds->GameMapPairs.Count)
				pds->PlaylistCycleIndex = 0;
			return true;
		});
	dtCyclePlaylist->Enable();
}

ServerConfig ServerUtils::LoadConfig(std::string configFilePath)
{
	ServerConfig config = {};

	// check if config file exists
	if(!fs::exists(configFilePath))
	{
		Log->warn("server config {} doesn't exist", configFilePath);

		// use default config file if exists; otherwise use builtin default
		std::string defaultConfigFile = moduleConfig.ConfigsPath + "default.json";
		if(!fs::exists(defaultConfigFile)) {
			Log->warn("default server config {} doesn't exist, using builtin default config", defaultConfigFile);
			configFilePath = "";
		} else {
			Log->info("loading default server config from {}", defaultConfigFile);
			configFilePath = defaultConfigFile;
		}
	}

	// parse config file if found
	if(!configFilePath.empty()) {
		try {
			config = Utils::ReadJsonFile<ServerConfig>(configFilePath);
		}
		catch (nlohmann::json::exception e) {
			Log->error("failed parsing {}, using builtin default config", configFilePath);
			Log->error(e.what());
		}
	}

	// map url params to config
	map_url_config(config.mutators, blre->URL);
	map_url_config(config.properties, blre->URL);
	map_url_config(config.webserver, blre->URL);
	return config;
}

void ServerUtils::SaveServerInfo()
{
	static auto serverInfoPath = moduleConfig.InfoFile;

	try
	{
		std::string serverInfo = "";

		WaitForSingleObject(infoMutex, INFINITE);
		serverInfo = nlohmann::json(this->info).dump(4);
		ReleaseMutex(infoMutex);

		std::ofstream output(serverInfoPath);
		if (!output.is_open()) {
			Log->error(fmt::format("failed writing server info to {0}", serverInfoPath));
			return;
		}

		output << serverInfo << std::endl;
		output.close();
	}
	catch (std::exception e)
	{
		Log->error(fmt::format("failed saving {0}", serverInfoPath));
		Log->error(e.what());
	};
}

void ServerUtils::UpdateServerInfo(AFoxGame* game)
{
	WaitForSingleObject(infoMutex, INFINITE);

	// @todo update the following infos on FoxIntermission.BeginTransitionToNewMap
	info.Map = game->WorldInfo->GetMapName(true).ToChar();
	info.ServerName = game->FGRI->ServerName.ToChar();
	info.Playlist = game->FGRI->playlistName.GetName();
	info.GameModeFullName = game->GameTypeString.ToChar();
	info.GameMode = game->GameTypeAbbreviatedName.ToChar();
	info.GoalScore = game->GoalScore;
	info.MaxPlayers = game->MaxPlayers;
	info.TimeLimit = game->FGRI->TimeLimit * 60;

	// @todo either keep here or provide timestamp of game start and calculate on clients
	info.RemainingTime = game->FGRI->RemainingTime;

	info.PlayerCount = 0;
	info.BotCount = 0;

	// @todo use FoxTeamInfo.AddToTeam and FoxTeamInfo.RemoveFromTeam to keep track of players/bots
	// use FoxIntermission.BeginTransitionToNewMap to populate team array
	auto teamsInfo = std::vector<TeamInfoStat>();
	for (auto team : game->Teams)
	{
		if (!serverConfig.webserver.ShowSpectatorTeam && team->TeamIndex == game->NumTeams - 1)
			continue;

		auto teamInfo = TeamInfoStat();
		teamInfo.TeamScore = team->Score;
		teamInfo.TeamIndex = team->TeamIndex;
		teamInfo.BotCount = team->BotSize;
		teamInfo.PlayerCount = team->HumanSize;
		teamInfo.TeamName = team->TeamName.ToChar();

		for (auto pri : game->GameReplicationInfo->PRIArray)
		{
			if (!pri->Team || team->TeamIndex != pri->Team->TeamIndex)
				continue;

			auto playerInfo = ActorInfoStat();
			playerInfo.Name = pri->PlayerName.ToChar();
			playerInfo.Kills = pri->Kills;
			playerInfo.Deaths = pri->Deaths;
			playerInfo.Score = pri->Score;

			if (pri->bBot)
				teamInfo.BotList.push_back(playerInfo);
			else
				teamInfo.PlayerList.push_back(playerInfo);
		}

		teamsInfo.push_back(teamInfo);
		info.PlayerCount += teamInfo.PlayerCount;
		info.BotCount += teamInfo.BotCount;
	}

	info.TeamList = teamsInfo;

	// @todo use FoxGame.FoxGRI.SetMutatorEnabled to keep track of mutators
	info.Mutators.DisableDepots = game->FGRI->IsMutatorEnabled(3);
	info.Mutators.DisableHRV = game->FGRI->IsMutatorEnabled(4);
	info.Mutators.DisableHeadShots = game->FGRI->IsMutatorEnabled(6);
	info.Mutators.StockLoadout = game->FGRI->IsMutatorEnabled(7);
	info.Mutators.DisablePrimaries = game->FGRI->IsMutatorEnabled(8);
	info.Mutators.DisableSecondaries = game->FGRI->IsMutatorEnabled(9);
	info.Mutators.DisableGear = game->FGRI->IsMutatorEnabled(10);
	info.Mutators.DisableTacticalGear = game->FGRI->IsMutatorEnabled(11);
	info.Mutators.DisableHealthRegen = game->FGRI->IsMutatorEnabled(12);
	info.Mutators.DisableElementalAmmo = game->FGRI->IsMutatorEnabled(13);
	info.Mutators.HeadshotsOnly = game->FGRI->IsMutatorEnabled(14);
	info.Mutators.StaminaModifier = game->FGRI->StaminaModifier;
	info.Mutators.HealthModifier = game->FGRI->HealthModifier;
	ReleaseMutex(infoMutex);

	SaveServerInfo();
}

void DETOUR_CB_CLS_IMPL(ServerUtils::UpdateGameSettingsCb, AFoxGame, UpdateGameSettings, game)
{
	Log->trace("Updating game settings");

	static bool firstExecution = true;
	if (firstExecution)
	{
		this->ApplyDefaultSettings();
		firstExecution = false;
	}

	// @todo apply settings only when necessary instead of on timer to reduce overload
	this->ApplySettings(game);

	// @todo move this to more specific callbacks so only the info which is changed has to be updated
	this->UpdateServerInfo(game);

	// @todo call this only if settings have changed
	//this->SaveServerInfo();

	detour->Continue();
}

void DETOUR_CB_CLS_IMPL(ServerUtils::ValidatePlayerLoadout, AFoxPC, ServerSetLoadout, pc)
{
	detour->Continue();
	this->LoadoutValidator.ValidatePlayerLoadout(pc);
}

bool DETOUR_CB_CLS_EVENT_IMPL(ServerUtils::SelectPlaylistByNameCb, UFoxDataStore_Playlists, SelectPlaylistByName, plds)
{
	detour->Continue();
	plds->RandomType = EPlaylistType::PlaylistType_Repeat;

	if (params->playlistName.Index == 0)
		return true;

	FName playlistName(params->playlistName.GetName());
	for (auto provider : plds->PlayListProviders) {
		if (provider->Name == playlistName) {
			provider->Name = params->playlistName;
			plds->CachedPlaylistInfo = provider;
			break;
		}
	}

	if (plds->CachedPlaylistInfo == nullptr)
		return false;

	plds->ResetCycleInfo();
	if (!plds->BuildPlaylistInfoByProvider(plds->CachedPlaylistInfo, &plds->MapProviders, &plds->GameProviders, &plds->PlaylistGameProviders)) {
		return false;
	}


	plds->BuildPlaylistCycleInfo(&plds->GameMapPairs);
	plds->LastLoadedPlaylistName = params->playlistName;
	return true;
}

void ServerUtils::ServerInfoRequestHandler(const httplib::Request& req, httplib::Response& res)
{
	WaitForSingleObject(infoMutex, INFINITE);
	nlohmann::json j = this->info;
	ReleaseMutex(infoMutex);

	res.status = 200;
	res.set_content(j.dump(4), "application/json");
}

void ServerUtils::ServerPatchesRequestHandler(const httplib::Request& req, httplib::Response& res)
{
	res.status = 200;
	res.set_content(blre->Patcher->GetPatchConfig().dump(), "application/json");
}

void ServerUtils::ApplyDefaultSettings()
{
	UFoxIntermission* intermission = UObject::GetInstanceOf<UFoxIntermission>(true);
	intermission->PlayerSearchTime = serverConfig.properties.PlayerSearchTime;
}

void ServerUtils::ApplySettings(AFoxGame* game)
{
	int currPlaylistIndex = game->PlaylistsDataStore->PlaylistCycleIndex;
	const ServerProperties& props = currPlaylistIndex < Playlist.size() ?
		Playlist[currPlaylistIndex].Properties : serverConfig.properties;

	// game->MaxIntermissionIdle = props.MaxIntermissionIdle;
	game->GameRespawnTime = props.GameRespawnTime;
	game->GameForceRespawnTime = props.GameForceRespawnTime;
	game->NumEnemyVotesRequiredForKick = props.NumEnemyVotesRequiredForKick;
	game->NumFriendlyVotesRequiredForKick = props.NumFriendlyVotesRequiredForKick;
	game->VoteKickBanSeconds = props.VoteKickBanSeconds;
	game->MaxIdleTime = props.MaxIdleTime;
	game->GoalScore = props.GoalScore;
	game->FGRI->GoalScore = props.GoalScore;
	game->NumBots = props.NumBots;
	game->FGRI->NumBots = props.NumBots;

	if (game->FGRI->GameStatus == EGameStatus::GS_Intermission) {
		UFoxIntermission* intermission = (UFoxIntermission*)game->FGRI->GameFlow;
		intermission->MinRequiredPlayersToStart = props.MinRequiredPlayersToStart;
		game->TimeLimit = props.TimeLimit;
		game->FGRI->TimeLimit = game->TimeLimit;
		game->FGRI->RemainingMinute = game->TimeLimit;
		game->FGRI->RemainingTime = game->TimeLimit * 60;
		game->FGRI->NumBots = props.NumBots;
		game->MaxBotCount = props.MaxBotCount;
		game->NumBots = props.NumBots;
		game->MaxPlayers = game->MaxPlayersAllowed = props.MaxPlayers;
	}

	// More mutators in Engine_classes.h line 2382
	game->FGRI->SetMutatorEnabled(3, serverConfig.mutators.DisableDepots);
	game->FGRI->SetMutatorEnabled(4, serverConfig.mutators.DisableHRV);
	game->FGRI->SetMutatorEnabled(6, serverConfig.mutators.DisableHeadShots);
	game->FGRI->SetMutatorEnabled(7, serverConfig.mutators.StockLoadout);
	game->FGRI->SetMutatorEnabled(8, serverConfig.mutators.DisablePrimaries);
	game->FGRI->SetMutatorEnabled(9, serverConfig.mutators.DisableSecondaries);
	game->FGRI->SetMutatorEnabled(10, serverConfig.mutators.DisableGear);
	game->FGRI->SetMutatorEnabled(11, serverConfig.mutators.DisableTacticalGear);
	game->FGRI->SetMutatorEnabled(12, serverConfig.mutators.DisableHealthRegen);
	game->FGRI->SetMutatorEnabled(13, serverConfig.mutators.DisableElementalAmmo);
	game->FGRI->SetMutatorEnabled(14, serverConfig.mutators.HeadshotsOnly);

	// "Custom" mutators
	game->FGRI->StaminaModifier = serverConfig.mutators.StaminaModifier;
	game->FGRI->HealthModifier = serverConfig.mutators.HealthModifier;
}

/*
*/

/**
 * Initialization of the module. 
 * 
 * @remark **Do not change the name or remove the __declspec(dllexport) from this function, otherwise BLRevive will fail to load the module!**
 * 
 * @param blre pointer to BLRevive API
*/
extern "C" __declspec(dllexport) void InitializeModule(BLRevive *blre)
{
	if (!Utils::IsServer()) {
		return;
	}

	ServerUtils* serverUtils = new ServerUtils(blre);
	serverUtils->Init();
}

BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved)
{
	return true;
}